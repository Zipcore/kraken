void InitSettings()
{
	if(g_aFeatures != null)
		return;
	
	g_aFeatures = new ArrayList(64);
	g_aSettings = new ArrayList(512);
}

bool GetSetting(char[] sFeature, char[] sSetting, int iLength)
{
	if(g_aFeatures == null)
	{
		LogError("Feature: %s can't get loaded. Settings not loaded yet.'", sFeature);
		return false;
	}
	
	int iIndex = FindStringInArray(g_aFeatures, sFeature);
	
	/* Update Cached Settings */
	if(iIndex == -1)
		return false;
	
	g_aSettings.GetString(iIndex, sSetting, iLength);
	return true;
}

void LoadSettings()
{
	InitSettings();
	
	char sQuery[512];
	FormatEx(sQuery, sizeof(sQuery), "SELECT feature, setting FROM `kraken`;");
	g_dMain.Query(Callback_LoadSettings, sQuery, _, DBPrio_High);
}

public void Callback_LoadSettings(Database db, DBResultSet results, const char[] error, any data)
{
	if (db == null) 
	{
		LogError("(Callback_LoadSettings) - %s", error);
		return;
	}
	
	// Cache Settings
	
	g_aSettings.Clear();
	g_aFeatures.Clear();

	while(results.FetchRow())
	{
		char sFeature[64];
		results.FetchString(0, sFeature, sizeof(sFeature));
		
		char sSetting[512];
		results.FetchString(1, sSetting, sizeof(sSetting));
		
		g_aSettings.PushString(sSetting);
		g_aFeatures.PushString(sFeature);
	}
	OnSettingsLoaded();
}

bool SetSetting(char[] sFeature, char[] sSetting)
{
	char sQuery[512];
	Format(sQuery, sizeof(sQuery), "INSERT INTO `kraken` (feature, setting) VALUES ('%s', '%s') ON DUPLICATE KEY UPDATE setting = '%s', updated_at = CURRENT_TIMESTAMP;", sFeature, sSetting, sSetting);
	Kraken_PushQuery(sQuery);
	
	int iIndex = FindStringInArray(g_aFeatures, sFeature);
	
	if(iIndex != -1)
	{
		g_aSettings.SetString(iIndex, sSetting);
		g_aFeatures.SetString(iIndex, sFeature);
	}
	else
	{
		g_aSettings.PushString(sSetting);
		g_aFeatures.PushString(sFeature);
	}
	
	return true;
}

/* Natives */

public int Native_RegisterSetting(Handle hPlugin, int iParams) 
{
	char sFeature[64];
	char sDefaultSetting[64];
	int iLength;
	
	if(GetNativeStringLength(1, iLength) != SP_ERROR_NONE)
	{
		LogError("Kraken_RegisterSetting: Couldn't get string length of it's first param.");
		return -1;
	}
	GetNativeString(1, sFeature, iLength);
	
	if(GetNativeStringLength(2, iLength) != SP_ERROR_NONE)
	{
		LogError("Kraken_RegisterSetting: Couldn't get string length of it's second param.");
		return -1;
	}
	GetNativeString(1, sDefaultSetting, iLength);
	
	int iFeatureID = g_aFeatures.FindString(sFeature);
	
	if(iFeatureID == -1)
	{
		iFeatureID = g_aFeatures.PushString(sFeature);
		g_aSettings.PushString(sDefaultSetting);
		
		SetSetting(sFeature, sDefaultSetting);
	}
	
	return iFeatureID;
}

public int Native_GetSettingInt(Handle hPlugin, int iParams) 
{
	int iFeatureID = GetNativeCell(1);
	
	if(!iFeatureID || iFeatureID >= g_aFeatures.Length)
	{
		LogError("Kraken_GetSettingInt: Invalid featureID.");
		return -1;
	}
	
	char sBuffer[32];
	g_aSettings.GetString(iFeatureID, sBuffer, sizeof(sBuffer));
	
	return StringToInt(sBuffer);
}

public int Native_GetSettingFloat(Handle hPlugin, int iParams) 
{
	int iFeatureID = GetNativeCell(1);
	
	if(!iFeatureID || iFeatureID >= g_aFeatures.Length)
	{
		LogError("Kraken_GetSettingFloat: Invalid featureID.");
		return false;
	}
	
	char sBuffer[32];
	g_aSettings.GetString(iFeatureID, sBuffer, sizeof(sBuffer));
	
	SetNativeCellRef(2, StringToFloat(sBuffer));
	return true;
}

public int Native_GetSettingString(Handle hPlugin, int iParams) 
{
	int iFeatureID = GetNativeCell(1);
	
	if(!iFeatureID || iFeatureID >= g_aFeatures.Length)
	{
		LogError("Kraken_GetSettingString: Invalid featureID.");
		return false;
	}
	
	int iSize = GetNativeCell(3);
	
	char[] sBuffer = new char[iSize];
	g_aSettings.GetString(iFeatureID, sBuffer, iSize);
	
	SetNativeString(2, sBuffer, iSize);
	return true;
}

public int Native_SetSettingInt(Handle hPlugin, int iParams) 
{
	int iFeatureID = GetNativeCell(1);
	
	if(!iFeatureID || iFeatureID >= g_aFeatures.Length)
	{
		LogError("Kraken_SetSettingInt: Invalid featureID.");
		return false;
	}
	
	int iSetting = GetNativeCell(2);
	
	char sSetting[32];
	IntToString(iSetting, sSetting, sizeof(sSetting));
	
	char sFeature[64];
	g_aFeatures.GetString(iFeatureID, sFeature, sizeof(sFeature));
	
	SetSetting(sFeature, sSetting);
	
	return true;
}

public int Native_SetSettingFloat(Handle hPlugin, int iParams) 
{
	int iFeatureID = GetNativeCell(1);
	
	if(!iFeatureID || iFeatureID >= g_aFeatures.Length)
	{
		LogError("Kraken_SetSettingFloat: Invalid featureID.");
		return false;
	}
	
	float fSetting = GetNativeCell(2);
	
	char sSetting[32];
	FloatToString(fSetting, sSetting, sizeof(sSetting));
	
	char sFeature[64];
	g_aFeatures.GetString(iFeatureID, sFeature, sizeof(sFeature));
	
	SetSetting(sFeature, sSetting);
	
	return true;
}

public int Native_SetSettingString(Handle hPlugin, int iParams) 
{
	int iFeatureID = GetNativeCell(1);
	
	if(!iFeatureID || iFeatureID >= g_aFeatures.Length)
	{
		LogError("Kraken_SetSettingString: Invalid featureID.");
		return false;
	}
	
	int iSize;
	
	if(GetNativeStringLength(2, iSize) != SP_ERROR_NONE)
	{
		LogError("Kraken_SetSettingString: Couldn't get string length of it's second param.");
		return false;
	}
	
	char[] sSetting = new char[iSize];
	GetNativeString(2, sSetting, iSize);
	
	char sFeature[64];
	g_aFeatures.GetString(iFeatureID, sFeature, sizeof(sFeature));
	SetSetting(sFeature, sSetting);
	return true;
}