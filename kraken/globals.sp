/* Database */

Database g_dMain = null;

bool g_bServerLoaded;

/* Features & Settings */

ArrayList g_aFeatures = null;
ArrayList g_aSettings = null;

/* Server Features & Settings */

ArrayList g_aServerFeatures = null;
ArrayList g_aServerSettings = null;

/* Forwards */

Handle g_fwdOnDatabaseConnected;
Handle g_fwdOnDatabasePong;
Handle g_fwdOnDatabaseDisconnect;
Handle g_fwdOnMapLoaded;
Handle g_fwdOnClientLoaded;
Handle g_fwdOnClientUnload;
Handle g_fwdOnVersionChecked;

ConVar g_cChatPrefix;
char g_sChatPrefix[64];

ConVar g_cChatPrefixAdmin;
char g_sChatPrefixAdmin[64];

/* Server */

int g_iServerID = -1;
int g_iServerGroupID = -1;
char g_sHostName[64];
int g_iHostPort;
char g_sHostIP[16];
int g_iServerSlots;

/* Player */

bool g_bUserLoaded[MAXPLAYERS + 1];
Handle g_hAuthTimer[MAXPLAYERS + 1] = { null, ... };

int g_iUserID[MAXPLAYERS + 1];
int g_iUserSessionID[MAXPLAYERS + 1];
char g_sAuth[MAXPLAYERS + 1][18];

char g_sIP[MAXPLAYERS + 1][16];
char g_sCCode[MAXPLAYERS + 1][4];

/* Map */

int g_iMapID = -1;
char g_sRawMap[64], g_sMap[PLATFORM_MAX_PATH];

/* Disconnect Reasons */

#define DCREASON_CONNECTED "Active"
#define DCREASON_DISCONNECT "Disconnect"
#define DCREASON_PLUGINSTART "Plugin Start"
#define DCREASON_PLUGINEND "Plugin End"