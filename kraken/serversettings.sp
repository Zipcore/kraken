void CreateServerSettingsTable()
{
	char sQuery[2048];
	Format(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS `server_settings`  (sid INT(11), feature VARCHAR(64) NOT NULL, setting VARCHAR(512) NOT NULL, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`sid`,`feature`), INDEX uid_index USING BTREE (sid), KEY (sid), fulltext (feature));");
	g_dMain.Query(Callback_CreateServerSettingsTable, sQuery, _, DBPrio_High);
}

public void Callback_CreateServerSettingsTable(Database db, DBResultSet results, const char[] error, any data) 
{
	if (db == null) 
	{
		LogError("(CreateServerSettingsTable) - %s", error);
		return;
	}
}

void InitServerSettings()
{
	if(g_aServerFeatures == null)
	{
		g_aServerSettings = new ArrayList(512);
		g_aServerFeatures = new ArrayList(64);
	}
	
	CreateServerSettingsTable();
}

void LoadServerSettings()
{
	InitSettings();
	
	char sQuery[512];
	FormatEx(sQuery, sizeof(sQuery), "SELECT feature, setting FROM `server_settings` WHERE sid = %i;", g_iServerID);
	g_dMain.Query(Callback_LoadServerSettings, sQuery, _, DBPrio_High);
}

public void Callback_LoadServerSettings(Database db, DBResultSet results, const char[] error, any data)
{
	if (db == null || results == null) 
	{
		LogError("(Callback_LoadServerSettings) - %s", error);
		return;
	}
	
	// Cache Settings
	
	g_aServerSettings.Clear();
	g_aServerFeatures.Clear();

	while(results.FetchRow())
	{
		char sFeature[64];
		results.FetchString(0, sFeature, sizeof(sFeature));
		
		char sSetting[512];
		results.FetchString(1, sSetting, sizeof(sSetting));
		
		g_aServerSettings.PushString(sSetting);
		g_aServerFeatures.PushString(sFeature);
	}
	
	OnServerSettingsLoaded();
}

bool SetServerSetting(char[] sFeature, char[] sSetting)
{
	char sQuery[512];
	Format(sQuery, sizeof(sQuery), "INSERT INTO `server_settings` (feature, setting) VALUES (%i, '%s', '%s') ON DUPLICATE KEY UPDATE setting = '%s', updated_at = CURRENT_TIMESTAMP;", g_iServerID, sFeature, sSetting, sSetting);
	Kraken_PushQuery(sQuery);
	
	int iIndex = FindStringInArray(g_aServerFeatures, sFeature);
	
	if(iIndex != -1)
	{
		g_aServerSettings.SetString(iIndex, sSetting);
		g_aServerFeatures.SetString(iIndex, sFeature);
	}
	else
	{
		g_aServerSettings.PushString(sSetting);
		g_aServerFeatures.PushString(sFeature);
	}
	
	return true;
}

/* Natives */

public int Native_RegisterServerSetting(Handle hPlugin, int iParams) 
{
	char sFeature[64];
	char sDefaultSetting[64];
	int iLength;
	
	if(GetNativeStringLength(1, iLength) != SP_ERROR_NONE)
	{
		LogError("Kraken_RegisterServerSetting: Couldn't get string length of it's first param.");
		return -1;
	}
	GetNativeString(1, sFeature, iLength);
	
	if(GetNativeStringLength(2, iLength) != SP_ERROR_NONE)
	{
		LogError("Kraken_RegisterServerSetting: Couldn't get string length of it's second param.");
		return -1;
	}
	GetNativeString(1, sDefaultSetting, iLength);
	
	int iFeatureID = g_aServerFeatures.FindString(sFeature);
	
	if(iFeatureID == -1)
	{
		iFeatureID = g_aServerFeatures.PushString(sFeature);
		g_aServerSettings.PushString(sDefaultSetting);
		
		SetServerSetting(sFeature, sDefaultSetting);
	}
	
	return iFeatureID;
}

public int Native_GetServerSettingInt(Handle hPlugin, int iParams) 
{
	int iFeatureID = GetNativeCell(1);
	
	if(!iFeatureID || iFeatureID >= g_aServerFeatures.Length)
	{
		LogError("Kraken_GetServerSettingInt: Invalid featureID.");
		return -1;
	}
	
	char sBuffer[32];
	g_aServerSettings.GetString(iFeatureID, sBuffer, sizeof(sBuffer));
	
	return StringToInt(sBuffer);
}

public int Native_GetServerSettingFloat(Handle hPlugin, int iParams) 
{
	int iFeatureID = GetNativeCell(1);
	
	if(!iFeatureID || iFeatureID >= g_aServerFeatures.Length)
	{
		LogError("Kraken_GetServerSettingFloat: Invalid featureID.");
		return false;
	}
	
	char sBuffer[32];
	g_aServerSettings.GetString(iFeatureID, sBuffer, sizeof(sBuffer));
	
	SetNativeCellRef(2, StringToFloat(sBuffer));
	return true;
}

public int Native_GetServerSettingString(Handle hPlugin, int iParams) 
{
	int iFeatureID = GetNativeCell(1);
	
	if(!iFeatureID || iFeatureID >= g_aServerFeatures.Length)
	{
		LogError("Kraken_GetServerSettingString: Invalid featureID.");
		return false;
	}
	
	int iSize = GetNativeCell(3);
	
	char[] sBuffer = new char[iSize];
	g_aServerSettings.GetString(iFeatureID, sBuffer, iSize);
	
	SetNativeString(2, sBuffer, iSize);
	return true;
}

public int Native_SetServerSettingInt(Handle hPlugin, int iParams) 
{
	int iFeatureID = GetNativeCell(1);
	
	if(!iFeatureID || iFeatureID >= g_aServerFeatures.Length)
	{
		LogError("Kraken_SetServerSettingInt: Invalid featureID.");
		return false;
	}
	
	int iSetting = GetNativeCell(2);
	
	char sSetting[32];
	IntToString(iSetting, sSetting, sizeof(sSetting));
	
	char sFeature[64];
	g_aServerFeatures.GetString(iFeatureID, sFeature, sizeof(sFeature));
	
	SetServerSetting(sFeature, sSetting);
	
	return true;
}

public int Native_SetServerSettingFloat(Handle hPlugin, int iParams) 
{
	int iFeatureID = GetNativeCell(1);
	
	if(!iFeatureID || iFeatureID >= g_aServerFeatures.Length)
	{
		LogError("Kraken_SetServerSettingFloat: Invalid featureID.");
		return false;
	}
	
	float fSetting = GetNativeCell(2);
	
	char sSetting[32];
	FloatToString(fSetting, sSetting, sizeof(sSetting));
	
	char sFeature[64];
	g_aServerFeatures.GetString(iFeatureID, sFeature, sizeof(sFeature));
	
	SetServerSetting(sFeature, sSetting);
	
	return true;
}

public int Native_SetServerSettingString(Handle hPlugin, int iParams) 
{
	int iFeatureID = GetNativeCell(1);
	
	if(!iFeatureID || iFeatureID >= g_aServerFeatures.Length)
	{
		LogError("Kraken_SetServerSettingString: Invalid featureID.");
		return false;
	}
	
	int iSize;
	
	if(GetNativeStringLength(2, iSize) != SP_ERROR_NONE)
	{
		LogError("Kraken_SetServerSettingString: Couldn't get string length of it's second param.");
		return false;
	}
	
	char[] sSetting = new char[iSize];
	GetNativeString(2, sSetting, iSize);
	
	char sFeature[64];
	g_aServerFeatures.GetString(iFeatureID, sFeature, sizeof(sFeature));
	SetServerSetting(sFeature, sSetting);
	return true;
}