void CheckPluginVersion()
{
	/* Get file version and check if it's installed */
	char sFileVersion[64];
	GetFileVersion(sFileVersion);
	
	/* Get feature name for db version */
	char sDBVersion[64];
	
	/* Is version already existent? */
	bool bExists = GetSetting("db_version", sDBVersion, sizeof(sDBVersion));
	
	/* Write version to database if newer */
	if (!bExists || strcmp(PLUGIN_VERSION, sDBVersion) == 1)
		UpdateVersion(PLUGIN_VERSION);
	
	/* Store plugin version */
	StoreVersionTofile();
	
	/* Plugin Version Ready*/
	OnPluginVersionChecked(PLUGIN_VERSION, sFileVersion, sDBVersion);
	
	Call_StartForward(g_fwdOnVersionChecked);
	Call_PushString(PLUGIN_VERSION);
	Call_PushString(sFileVersion);
	Call_PushString(sDBVersion);
	Call_Finish();
}

bool GetFileVersion(char sVersion[64])
{
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), "data/kraken/kraken_version");
	
	Handle hFile = OpenFile(sPath, "r");
	if (hFile != null)
	{
		/* Read version from file */
		ReadFileLine(hFile, sVersion, sizeof(sVersion));
		delete hFile;
		return false;
	}
	
	return true;
}

void UpdateVersion(char[] sVersion)
{
	char sQuery[512];
	Format(sQuery, sizeof(sQuery), "INSERT INTO `kraken` (feature, setting) VALUES ('db_version', '%s') ON DUPLICATE KEY UPDATE setting = '%s', updated_at = CURRENT_TIMESTAMP;", sVersion, sVersion);
	Kraken_PushQuery(sQuery);
}

void StoreVersionTofile()
{
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), "data/kraken/kraken_version");
	
	Handle hFile = OpenFile(sPath, "w");
	if (hFile != null)
	{
		WriteFileLine(hFile, PLUGIN_VERSION);
		delete hFile;
	}
}