public void CreateConvars()
{
	g_cChatPrefix = CreateConVar("kraken_chatprefix", "{lime}[{darkblue}Kraken{lime}]{lime}", "Prefix to use for chat messages");
	g_cChatPrefixAdmin = CreateConVar("kraken_chatprefix_admin", "{lime}[{darkred}Kraken{lime}]{lime}", "Prefix to use for admin chat messages");

	AutoExecConfig(true, "kraken");

	GetConVarString(g_cChatPrefix, g_sChatPrefix, sizeof(g_sChatPrefix));
	GetConVarString(g_cChatPrefixAdmin, g_sChatPrefixAdmin, sizeof(g_sChatPrefixAdmin));

	HookConVarChange(g_cChatPrefix, OnConVarChanged);
	HookConVarChange(g_cChatPrefixAdmin, OnConVarChanged);
}

public void OnConVarChanged(ConVar convar, const char[] oldValue, const char[] newValue)
{
	if (convar == g_cChatPrefix)
		GetConVarString(convar, g_sChatPrefix, sizeof(g_sChatPrefix));
	else if (convar == g_cChatPrefixAdmin)
		GetConVarString(convar, g_sChatPrefixAdmin, sizeof(g_sChatPrefixAdmin));
}