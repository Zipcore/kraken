#include "kraken/player/auth.sp"
#include "kraken/player/disconnect.sp"
#include "kraken/player/session.sp"
#include "kraken/player/names.sp"
#include "kraken/player/natives.sp"

void InitUser()
{
	char sQuery[2048];
	
	FormatEx(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS `players` (uid INT NOT NULL AUTO_INCREMENT, steamid VARCHAR(17) NOT NULL, firstjoin TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`steamid`), INDEX uid_index USING BTREE (uid), KEY (steamid));");
	Kraken_PushQuery(sQuery);
	
	FormatEx(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS `player_names` (uid INT NOT NULL, name VARCHAR(64) NOT NULL, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, last_used TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`uid`, `name`), INDEX uid_index USING BTREE (uid), KEY (uid), fulltext (name)) DEFAULT CHARSET = utf8mb4;");
	Kraken_PushQuery(sQuery);
	
	FormatEx(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS `player_session` (id INT NOT NULL AUTO_INCREMENT, uid INT NOT NULL, sid INT NOT NULL, ip VARCHAR(15) NOT NULL, dcreason VARCHAR(128) DEFAULT '', country VARCHAR(4) NOT NULL, start TIMESTAMP DEFAULT CURRENT_TIMESTAMP, end TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`), INDEX uid_index USING BTREE (uid), INDEX sid_index USING BTREE (sid));");
	Kraken_PushQuery(sQuery);
	
	FormatEx(sQuery, sizeof(sQuery), "UPDATE `player_session` SET end = CURRENT_TIMESTAMP, dcreason = '%s' WHERE dcreason = '%s';", DCREASON_PLUGINSTART, DCREASON_CONNECTED);
	Kraken_PushQuery(sQuery);
}