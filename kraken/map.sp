void InitMap()
{
	char sQuery[2048];
	
	FormatEx(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS `maps` (mid INT NOT NULL AUTO_INCREMENT, name VARCHAR(64) NOT NULL, PRIMARY KEY (`name`), INDEX mid_index USING BTREE (mid), fulltext (name));");
	Kraken_PushQuery(sQuery);
}

void MapStart()
{
	g_iMapID = -1;
	
	GetCurrentMap(g_sMap, sizeof(g_sMap));
	GetMapDisplayName(g_sMap, g_sRawMap, sizeof(g_sRawMap));
	
	LoadMapID();
}

public void LoadMapID() 
{
	if(!g_bServerLoaded)
		return;
	
	char sBuffer[512];
	FormatEx(sBuffer, sizeof(sBuffer), "SELECT mid FROM `maps` WHERE name = '%s';", g_sRawMap);
	g_dMain.Query(Callback_LoadMapID, sBuffer, _, DBPrio_High);
}

public void Callback_LoadMapID(Database db, DBResultSet results, const char[] error, int data)
{
	if (db == null) 
	{
		LogError("(LoadMapID) - %s", error);
		return;
	}
	
	if(results.FetchRow())
	{
		g_iMapID = results.FetchInt(0);
		
		Call_StartForward(g_fwdOnMapLoaded);
		Call_PushCell(g_iMapID);
		Call_Finish();
		
		return;
	}
	
	g_iMapID = -1;
	
	InsertMap();
	LoadMapID();
}

void InsertMap()
{
	char sBuffer[512];
	
	FormatEx(sBuffer, sizeof(sBuffer), "INSERT INTO `maps` (name) VALUES ('%s') ON DUPLICATE KEY UPDATE `mid` = `mid`;", g_sRawMap);
	g_dMain.Query(Callback_InsertMap, sBuffer, _, DBPrio_High);
}

public void Callback_InsertMap(Database db, DBResultSet results, const char[] error, int data)
{
	if (db == null) 
	{
		LogError("(InsertMap) - %s", error);
		return;
	}
}

/* Natives */

public int Native_GetMapID(Handle hPlugin, int iParams) 
{
	return g_iMapID;
}
