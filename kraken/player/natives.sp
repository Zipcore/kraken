public int Native_GetUserID(Handle hPlugin, int iParams) 
{
	int iClient = GetNativeCell(1);

	if (Kraken_IsValidClient(iClient))
		return g_iUserID[iClient];

	return 0;
}

public int Native_GetUserSessionID(Handle hPlugin, int iParams) 
{
	int iClient = GetNativeCell(1);

	if (Kraken_IsValidClient(iClient))
		return g_iUserSessionID[iClient];

	return 0;
}