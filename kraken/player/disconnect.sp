public void Event_OnDisconnect(Event event, const char[] name, bool dontBroadcast)
{
	int iClient = GetClientOfUserId(event.GetInt("userid"));
	
	if(iClient > 0 && g_iUserSessionID[iClient] > 0)
	{
		char sName[32];
		event.GetString("name", sName, sizeof(sName), "Unknown");
		
		char sReason[128];
		event.GetString("reason", sReason, sizeof(sReason), "Unknown");
		
		#if defined DEBUG
			PrintToServer("%N left the server. Reason: %s", iClient, sReason);
		#endif
		
		char sReasonSafe[257];
		g_dMain.Escape(sReason, sReasonSafe, 257);
		
		char sQuery[512];
		Format(sQuery, sizeof(sQuery), "UPDATE `player_session` SET end = CURRENT_TIMESTAMP, dcreason = '%s' WHERE id = '%i';", sReasonSafe, g_iUserSessionID[iClient]);
		Kraken_PushQuery(sQuery);
			
		Call_StartForward(g_fwdOnClientUnload);
		Call_PushCell(iClient);
		Call_PushCell(g_iUserID[iClient]);
		Call_PushCell(g_iUserSessionID[iClient]);
		Call_PushString(sReasonSafe);
		Call_Finish();
		
		g_iUserSessionID[iClient] = 0;
	}
}

public void OnClientDisconnect(int iClient) 
{
	if(g_hAuthTimer[iClient] != null)
	{
		delete g_hAuthTimer[iClient];
		g_hAuthTimer[iClient] = null;
	}
	
	if(g_iUserSessionID[iClient] > 0)
	{
		char sQuery[512];
			
		Format(sQuery, sizeof(sQuery), "UPDATE `player_session` SET end = CURRENT_TIMESTAMP, dcreason = '%s' WHERE id = '%i';", DCREASON_DISCONNECT, g_iUserSessionID[iClient]);
		Kraken_PushQuery(sQuery);
			
		Call_StartForward(g_fwdOnClientUnload);
		Call_PushCell(iClient);
		Call_PushCell(g_iUserID[iClient]);
		Call_PushCell(g_iUserSessionID[iClient]);
		Call_PushString(DCREASON_DISCONNECT);
		Call_Finish();
	}
	
	g_iUserID[iClient] = 0;
	g_iUserSessionID[iClient] = 0;
	g_bUserLoaded[iClient] = false;
}