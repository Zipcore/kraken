void UpdatePlayerName(int iClient)
{
	char sName[64];
	GetClientName(iClient, sName, sizeof(sName));
	
	char sSafeName[129];
	g_dMain.Escape(sName, sSafeName, sizeof(sSafeName));
	
	char sQuery[512];
	Format(sQuery, sizeof(sQuery), "INSERT INTO `player_names` (uid, name) VALUES (%i, '%s') ON DUPLICATE KEY UPDATE last_used = CURRENT_TIMESTAMP;", g_iUserID[iClient], sSafeName);
	Kraken_PushQuery(sQuery);
}