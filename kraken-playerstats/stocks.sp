stock bool IsWarmup()
{
	return GameRules_GetProp("m_bWarmupPeriod") == 1;
}