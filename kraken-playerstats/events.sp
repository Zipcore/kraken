void HookEvents()
{
	HookEvent("round_end", Event_RoundEnd);
	HookEvent("round_mvp", Event_MVP);
	
	HookEvent("player_spawn", Event_PlayerSpawn);
	HookEvent("player_death", Event_PlayerDeath);
	HookEvent("player_hurt", Event_PlayerHurt);
	
	HookEvent("bomb_exploded", Event_BombExploded);
	HookEvent("bomb_planted", Event_BombPlanted);
	HookEvent("bomb_defused", Event_BombDefused);
	HookEvent("bomb_dropped", Event_BombDropped);
	
	HookEvent("weapon_fire", Event_WeaponFire);
	
	HookEvent("player_blind", Event_PlayerBlind);
	HookEvent("flashbang_detonate", Event_FlashbangDetonate);
}

public void Event_RoundEnd(Event eEvent, char[] sName, bool bDontBroadcast) 
{
	if(IsWarmup())
		return;
	
	for (int i = 1; i <= MaxClients; i++)
	{
		if(!IsClientInGame(i))
			continue;
		
		if(!IsPlayerAlive(i))
			continue;
		
		UpdateStats(i, "survived", 1);
	}
}

public void Event_MVP(Event eEvent, char[] sName, bool bDontBroadcast) 
{
	if(IsWarmup())
		return;
	
	int iClient = GetClientOfUserId(eEvent.GetInt("userid"));
	UpdateStats(iClient, "mvp", 1);
}

public void Event_PlayerSpawn(Event eEvent, char[] sName, bool bDontBroadcast) 
{
	if(IsWarmup())
		return;
	
	int iClient = GetClientOfUserId(eEvent.GetInt("userid"));
	
	UpdateStats(iClient, "spawns", 1);
}

public void Event_PlayerDeath(Event eEvent, char[] sName, bool bDontBroadcast) 
{
	if(IsWarmup())
		return;
	
	int iClient = GetClientOfUserId(eEvent.GetInt("userid"));
	
	UpdateStats(iClient, "death", 1);
	
	char sWeapon[64];
	GetEventString(eEvent, "weapon", sWeapon, sizeof(sWeapon));
	
	int iAttacker = GetClientOfUserId(eEvent.GetInt("attacker"));
	
	if(iAttacker == 0 || iAttacker == iClient)
	{
		UpdateStats(iAttacker, "suicide", 1);
	}
	else 
	{
		UpdateStats(iAttacker, "kill", 1);
		
		if(GetClientTeam(iClient) == GetClientTeam(iAttacker))
			UpdateStats(iAttacker, "teamkill", 1);
	
		if(eEvent.GetBool("headshot"))
			UpdateStats(iAttacker, "headshot", 1);
	
		if(eEvent.GetInt("penetrated") > 0)
			UpdateStats(iAttacker, "wallbang", 1);
		
		if(StrContains(sWeapon, "knife") != -1 || StrContains(sWeapon, "bayonet") != -1)
			UpdateStats(iAttacker, "knifekill", 1);
	}
	
	int iAssister = GetClientOfUserId(eEvent.GetInt("assister"));
	UpdateStats(iAssister, "assist", 1);
}

public void Event_PlayerHurt(Event eEvent, char[] sName, bool bDontBroadcast) 
{
	if(IsWarmup())
		return;
	
	int iVictim = GetClientOfUserId(eEvent.GetInt("userid"));
	int iAttacker = GetClientOfUserId(eEvent.GetInt("attacker"));
	int iDamage = eEvent.GetInt("dmg_health");
	
	if(iVictim <= 0 || iAttacker <= 0)
		return;
	
	g_iDamageInTemp[iVictim] += iDamage;
	
	if(iVictim == iAttacker)
		return;
	
	if(GetClientTeam(iVictim) == GetClientTeam(iAttacker))
		return;
	
	g_iDamageOutTemp[iAttacker] += iDamage;
	g_iHitTemp[iAttacker]++;
	
	int iHitgroup = eEvent.GetInt("hitgroup");
	g_iHitGroupTemp[iAttacker][iHitgroup]++;
}

/* Bomb Events*/

public void Event_BombExploded(Event eEvent, const char[] name, bool dontBroadcast)
{
	if(IsWarmup())
		return;
	
	int iClient = GetClientOfUserId(eEvent.GetInt("userid"));
	UpdateStats(iClient, "bomb_exp", 1);
}

public void Event_BombPlanted(Event eEvent, const char[] name, bool dontBroadcast)
{
	if(IsWarmup())
		return;
	
	int iClient = GetClientOfUserId(eEvent.GetInt("userid"));
	UpdateStats(iClient, "bomb_plt", 1);
}

public void Event_BombDefused(Event eEvent, const char[] name, bool dontBroadcast)
{
	if(IsWarmup())
		return;
	
	int iClient = GetClientOfUserId(eEvent.GetInt("userid"));
	UpdateStats(iClient, "bomb_def", 1);
}

public void Event_BombDropped(Event eEvent, const char[] name, bool dontBroadcast)
{
	if(IsWarmup())
		return;
	
	int iClient = GetClientOfUserId(eEvent.GetInt("userid"));
	UpdateStats(iClient, "bomb_drp", 1);
}

/* Weapon Fire */

public void Event_WeaponFire(Event eEvent, const char[] name, bool dontBroadcast)
{
	if(IsWarmup())
		return;
	
	int iClient = GetClientOfUserId(eEvent.GetInt("userid"));
	
	if(iClient > 0)
		g_iShotFiredTemp[iClient]++;
}

/* Flashstats (Credits: mukunda) */

#define DURATION_LIMIT 2.0
#define ALPHA_LIMIT 255.0

int g_iFlasher = 0;

float g_fFlashDelay;

int g_iTeamflashes;
float g_fTeamflash_total;

int g_iEnemyflashes;
float g_fEnemyflash_total;

public void Event_PlayerBlind(Event eEvent, const char[] name, bool dontBroadcast)
{
	if(IsWarmup())
		return;
	
	if(g_iFlasher <= 0)
		return;
	
	int iVictim = GetClientOfUserId(eEvent.GetInt("userid"));
	
	if(iVictim <= 0)
		return;
	
	bool alive = IsPlayerAlive(iVictim);
	
	float alpha = GetEntPropFloat(iVictim, Prop_Send, "m_flFlashMaxAlpha");
	float duration = GetEntPropFloat(iVictim, Prop_Send, "m_flFlashDuration");
	
	g_iFlashedTemp[iVictim]++;
	
	if(alpha >= ALPHA_LIMIT && duration >= DURATION_LIMIT)
	{
		if(!alive || g_iFlasher == iVictim)
			return;
		
		if(GetClientTeam(g_iFlasher) == GetClientTeam(iVictim))
		{
			g_iTeamflashes++;
			g_fTeamflash_total += duration;
			return;
		}
		
		g_iEnemyflashes++;
		g_fEnemyflash_total += duration;
	}
}

public void Event_FlashbangDetonate(Event eEvent, const char[] name, bool dontBroadcast)
{				
	if(IsWarmup())
		return;
				 	
	int iClient = GetClientOfUserId(eEvent.GetInt("userid"));
	
	if(g_iFlasher != 0)
	{
		g_fFlashDelay += 0.1;
		CreateTimer(g_fFlashDelay+0.1, ProcessFlashResultsDelayed);
	}
	else
	{
		ProcessFlashResults();
		g_iFlasher = 0;
	}
	
	g_iFlasher = iClient; 
	
	g_iTeamflashes = 0;
	g_fTeamflash_total = 0.0;
	
	g_iEnemyflashes = 0;
	g_fEnemyflash_total = 0.0; 
}

public Action ProcessFlashResultsDelayed(Handle hTimer)
{
	g_fFlashDelay -= 0.1;
	
	if(g_fFlashDelay <= 0.0)
		g_fFlashDelay = 0.0;
	
	ProcessFlashResults();
	return Plugin_Handled;
}

void ProcessFlashResults()
{
	if(g_iFlasher <= 0)
		return;
	
	if(g_iTeamflashes != 0)
		g_iFlashedTeamTemp[g_iFlasher] += g_iTeamflashes;
	
	if(g_iEnemyflashes != 0)
		g_iFlashedEnemyTemp[g_iFlasher] += g_iEnemyflashes;
	
	g_iFlasher = 0;
}
