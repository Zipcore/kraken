void UpdateTemp(int iClient)
{
	// Shots fired
	if(g_iShotFiredTemp[iClient] > 0)
	{
		UpdateStats(iClient, "shot", g_iShotFiredTemp[iClient]);
		g_iShotFiredTemp[iClient] = 0;
	}
	
	// Damage In
	if(g_iDamageInTemp[iClient] > 0)
	{
		UpdateStats(iClient, "dmg_in", g_iDamageInTemp[iClient]);
		g_iDamageInTemp[iClient] = 0;
	}
	
	// Damage Out
	if(g_iDamageOutTemp[iClient] > 0)
	{
		UpdateStats(iClient, "dmg_out", g_iDamageOutTemp[iClient]);
		g_iDamageOutTemp[iClient] = 0;
	}
	
	// Hits
	if(g_iHitTemp[iClient] > 0)
	{
		UpdateStats(iClient, "hit", g_iHitTemp[iClient]);
		g_iHitTemp[iClient] = 0;
	}
	
	// Hitgroup: Body
	if(g_iHitGroupTemp[iClient][0] > 0)
	{
		UpdateStats(iClient, "hit_body", g_iHitGroupTemp[iClient][0]);
		g_iHitGroupTemp[iClient][0] = 0;
	}
	
	// Hitgroup: 
	if(g_iHitGroupTemp[iClient][1] > 0)
	{
		UpdateStats(iClient, "hit_hs", g_iHitGroupTemp[iClient][1]);
		g_iHitGroupTemp[iClient][1] = 0;
	}
	
	// Hitgroup: 
	if(g_iHitGroupTemp[iClient][2] > 0)
	{
		UpdateStats(iClient, "hit_upt", g_iHitGroupTemp[iClient][2]);
		g_iHitGroupTemp[iClient][2] = 0;
	}
	
	// Hitgroup: 
	if(g_iHitGroupTemp[iClient][3] > 0)
	{
		UpdateStats(iClient, "hit_lot", g_iHitGroupTemp[iClient][3]);
		g_iHitGroupTemp[iClient][3] = 0;
	}
	
	// Hitgroup: 
	if(g_iHitGroupTemp[iClient][4] > 0)
	{
		UpdateStats(iClient, "hit_larm", g_iHitGroupTemp[iClient][4]);
		g_iHitGroupTemp[iClient][4] = 0;
	}
	
	// Hitgroup: 
	if(g_iHitGroupTemp[iClient][5] > 0)
	{
		UpdateStats(iClient, "hit_rarm", g_iHitGroupTemp[iClient][5]);
		g_iHitGroupTemp[iClient][5] = 0;
	}
	
	// Hitgroup: 
	if(g_iHitGroupTemp[iClient][6] > 0)
	{
		UpdateStats(iClient, "hit_lleg", g_iHitGroupTemp[iClient][6]);
		g_iHitGroupTemp[iClient][6] = 0;
	}
	
	// Hitgroup: 
	if(g_iHitGroupTemp[iClient][7] > 0)
	{
		UpdateStats(iClient, "hit_rleg", g_iHitGroupTemp[iClient][7]);
		g_iHitGroupTemp[iClient][7] = 0;
	}
	
	// Flashed
	if(g_iFlashedTemp[iClient] > 0)
	{
		UpdateStats(iClient, "flashed", g_iFlashedTemp[iClient]);
		g_iFlashedTemp[iClient] = 0;
	}
	
	// Flashed Team
	if(g_iFlashedTeamTemp[iClient] > 0)
	{
		UpdateStats(iClient, "flash_team", g_iFlashedTeamTemp[iClient]);
		g_iFlashedTeamTemp[iClient] = 0;
	}
	
	// Flashed Enemy
	if(g_iFlashedEnemyTemp[iClient] > 0)
	{
		UpdateStats(iClient, "flash_enmy", g_iFlashedEnemyTemp[iClient]);
		g_iFlashedEnemyTemp[iClient] = 0;
	}
}