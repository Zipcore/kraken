#define PLUGIN_VERSION "1.0"

#define DEBUG

#include <sourcemod>
#include <sdkhooks>
#include <kraken-core>

bool g_bKrakenCore;
bool g_bLate;

public Plugin myinfo =
{
	name = "Kraken - Command logger",
	author = "Zipcore, Credits: El Diablo",
	description = "Simple command logger for Kraken",
	version = PLUGIN_VERSION,
	url = "www.zipcore.net",
};

/* AdminCMD logging */

char g_sIgnoreCommands[][] = { // commands to ignore
	"+",
	"-",
	"sm_admin",
};

char g_sIncludeCommands[][] = { // commands to include
	"sm_calladmin"
};

public APLRes AskPluginLoad2(Handle hMyself, bool bLate, char[] sError, int iMaxError) 
{
	RegPluginLibrary("kraken-command-logger");
	Kraken_MarkNativesAsOptional();
	
	g_bLate = bLate;

	return APLRes_Success;
}

public void OnPluginStart() 
{
	if(LibraryExists("kraken-core"))
		g_bKrakenCore = true;
	
	HookAdminCmds();
	
	if(g_bLate)
	{
		g_bLate = false;
		CreateTable();
	}
}

public void OnLibraryAdded(const char[] name)
{
	if(StrEqual(name, "kraken-core"))
		g_bKrakenCore = true;
}

public void OnLibraryRemoved(const char[] name)
{
	if(StrEqual(name, "kraken-core"))
		g_bKrakenCore = false;
}

public void Kraken_OnDatabaseConnected(Database dDatabase, int iSID, int iGID, char[] sServerName)
{
	CreateTable();
}

void CreateTable()
{
	if(!g_bKrakenCore || !Kraken_IsDatabaseLoaded())
		return;
	
	Kraken_PushQuery("CREATE TABLE IF NOT EXISTS `command_log` (id INT NOT NULL AUTO_INCREMENT, sid INT NOT NULL, uid INT NOT NULL, usid INT NOT NULL, cmd VARCHAR(256) NOT NULL, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`), INDEX sid_index USING BTREE (sid), INDEX uid_index USING BTREE (uid), INDEX usid_index USING BTREE (usid));");
}

public void OnAllPluginsLoaded()
{
	HookAdminCmds(); // We call it a 2nd time here just in case, still won't hook plugins which are late loaded
}

void HookAdminCmds()
{
	char sCommand[256];
	char sDesc[256];
	int iFlags;
	Handle CmdIter = GetCommandIterator();
	
	while(ReadCommandIterator(CmdIter, sCommand, sizeof(sCommand), iFlags, sDesc, sizeof(sDesc)))
	{
		if (!HasIgnoreCommands(sCommand))
		{
			if (	(	iFlags & ADMFLAG_RESERVATION)	||	(iFlags & ADMFLAG_GENERIC)
				||	(iFlags & ADMFLAG_KICK) 			||	(iFlags & ADMFLAG_BAN)
				||	(iFlags & ADMFLAG_UNBAN)			||	(iFlags & ADMFLAG_SLAY)
				||	(iFlags & ADMFLAG_CHANGEMAP)	||	(iFlags & ADMFLAG_CONVARS)
				||	(iFlags & ADMFLAG_CONFIG)			||	(iFlags & ADMFLAG_CHAT)
				||	(iFlags & ADMFLAG_VOTE)			||	(iFlags & ADMFLAG_PASSWORD)
				||	(iFlags & ADMFLAG_RCON)			||	(iFlags & ADMFLAG_CHEATS) 
				||	(iFlags & ADMFLAG_CUSTOM1)		||	(iFlags & ADMFLAG_CUSTOM2)
				||	(iFlags & ADMFLAG_CUSTOM3)		||	(iFlags & ADMFLAG_CUSTOM4)
				||	(iFlags & ADMFLAG_CUSTOM5)		||	(iFlags & ADMFLAG_CUSTOM6)
				)
				AddCommandListener(Log_Admin_Command, sCommand);
			else if ((iFlags & ADMFLAG_ROOT))
					AddCommandListener(Log_Admin_Command, sCommand);
			else if (HasIncludeCommands(sCommand))
				AddCommandListener(Log_Admin_Command, sCommand);
		}
	}
	delete CmdIter;
}

public Action Log_Admin_Command(int iClient, const char[] sCommand, int iArgs)
{
	if(!g_bKrakenCore || !Kraken_IsDatabaseLoaded())
		return Plugin_Continue;
	
	if(!Kraken_IsDatabaseLoaded())
		return Plugin_Continue;
	
	if(Kraken_IsValidClient(iClient))
	{
		int iUserID = Kraken_GetUserID(iClient);
		int iUserSessionID = Kraken_GetUserSessionID(iClient);
		
		if(iUserID <= 0 || iUserSessionID <= 0)
			return Plugin_Continue;
		
		// Just in case the AddCommandListener added some command it wasn't suppose to via ADMFLAG_ROOT
		// for some reason commands that are registered via RegConsoleCmd without any flags specificed it will
		// assume it is ADMFLAG_ROOT???
		int iFlags = GetCommandFlags(sCommand);
		if (CheckCommandAccess(iClient, sCommand, iFlags))
		{
			// whole command string
			char CmdBuffer[255];
			GetCmdArgString(CmdBuffer, sizeof(CmdBuffer));

			// filter out any unicode
			FilterSentence(CmdBuffer, false, false);
			
			char sQuery[2048];
			Format(sQuery, sizeof(sQuery), "INSERT INTO `command_log` (sid,uid,usid,cmd) VALUES (%i, %i, %i, '%s %s');", Kraken_GetServerID(), iUserID, iUserSessionID, sCommand, CmdBuffer);
			Kraken_PushQuery(sQuery);
		}
	}

	return Plugin_Continue;
}

bool HasIgnoreCommands(const char sCheckCommand[256])
{
	for(int i = 0; i < sizeof(g_sIgnoreCommands); i++)
	{
		if(StrContains(sCheckCommand, g_sIgnoreCommands[i])==0)
			return true;
	}
	return false;
}

bool HasIncludeCommands(const char sCheckCommand[256])
{
	for(int i = 0; i < sizeof(g_sIncludeCommands); i++)
	{
		if(StrContains(sCheckCommand, g_sIncludeCommands[i])==0)
			return true;
	}
	return false;
}

stock void FilterSentence(char[] message, bool extremefilter = false, bool RemoveWhiteSpace = false)
{
	int charMax = strlen(message);
	int charIndex;
	int copyPos = 0;

	char strippedString[192];

	for (charIndex = 0; charIndex < charMax; charIndex++)
	{
		// Reach end of string. Break.
		if (message[copyPos] == 0) 
		{
			strippedString[copyPos] = 0;
			break;
		}

		if (GetCharBytes(message[charIndex])>1)
			continue;

		if(RemoveWhiteSpace && IsCharSpace(message[charIndex]))
			continue;

		if(extremefilter && IsAlphaNumeric(message[charIndex]))
		{
			strippedString[copyPos] = message[charIndex];
			copyPos++;
			continue;
		}

		// Found a normal character. Copy.
		if (!extremefilter && IsNormalCharacter(message[charIndex])) 
		{
			strippedString[copyPos] = message[charIndex];
			copyPos++;
			continue;
		}
	}

	// Copy back to passing parameter.
	strcopy(message, 192, strippedString);
}

stock bool IsAlphaNumeric(int characterNum) {
	return ((characterNum >= 48 && characterNum <=57)
		||  (characterNum >= 65 && characterNum <=90)
		||  (characterNum >= 97 && characterNum <=122));
}

stock bool IsNormalCharacter(int characterNum) {
	return (characterNum > 31 && characterNum < 127);
}
