/* Database */

forward void Kraken_OnDatabaseConnected(Database dDatabase, int iSID, int iGID, char[] sServerName);
forward void Kraken_OnDatabasePong(Database dDatabase, int iSID, int iGID, char[] sServerName);
forward void Kraken_OnDatabaseDisconnect();
forward void Kraken_OnVersionChecked(char[] sPluginVersion, char[] sFileVersion, char[] sDbVersion);

native bool Kraken_IsDatabaseLoaded();
native Database Kraken_GetDatabase();
native bool Kraken_PingDatabase();
native bool Kraken_PushQuery(char[] sQuery);

/* Server */

native int Kraken_GetServerID();
native int Kraken_GetServerGroupID();
native int Kraken_GetServerSlots();
native void Kraken_GetServerName(char[] sBuffer, int iLength);
native void Kraken_GetServerIP(char[] sBuffer, int iLength);
native int Kraken_GetServerPort();

/* Global Settings */

native int Kraken_RegisterSetting(char[] sFeature, char[] sDefaultSetting);
native int Kraken_GetSettingInt(int iFeatureID);
native bool Kraken_GetSettingFloat(int iFeatureID, float &fValue);
native bool Kraken_GetSettingString(int iFeatureID, char[] sValue, int iMaxlen);
native bool Kraken_SetSettingInt(int iFeatureID, int iValue);
native bool Kraken_SetSettingFloat(int iFeatureID, float fValue);
native bool Kraken_SetSettingString(int iFeatureID, char[] sValue);

/* Server Settings */

native int Kraken_RegisterServerSetting(char[] sFeature, char[] sDefaultSetting);
native int Kraken_GetServerSettingInt(int iFeatureID);
native bool Kraken_GetServerSettingFloat(int iFeatureID, float &fValue);
native bool Kraken_GetServerSettingString(int iFeatureID, char[] sValue, int iMaxlen);
native bool Kraken_SetServerSettingInt(int iFeatureID, int iValue);
native bool Kraken_SetServerSettingFloat(int iFeatureID, float fValue);
native bool Kraken_SetServerSettingString(int iFeatureID, char[] sValue);

/* Maps */

forward void Kraken_OnMapLoaded(int iMID);

native int Kraken_GetMapID();

/* Player */

forward void Kraken_OnClientLoaded(int iClient, int iUserID, int iSessionID);
forward void Kraken_OnClientUnload(int iClient, int iUserID, int iSessionID, char[] sDcReason);

native int Kraken_GetUserID(int iClient);
native int Kraken_GetUserSessionID(int iClient);


stock void Kraken_MarkNativesAsOptional()
{
	MarkNativeAsOptional("Kraken_IsDatabaseLoaded");
	MarkNativeAsOptional("Kraken_GetDatabase");
	MarkNativeAsOptional("Kraken_PingDatabase");
	MarkNativeAsOptional("Kraken_PushQuery");
	MarkNativeAsOptional("Kraken_GetServerID");
	MarkNativeAsOptional("Kraken_GetServerGroupID");
	MarkNativeAsOptional("Kraken_GetServerSlots");
	MarkNativeAsOptional("Kraken_GetServerName");
	MarkNativeAsOptional("Kraken_GetServerIP");
	MarkNativeAsOptional("Kraken_GetServerPort");
	
	MarkNativeAsOptional("Kraken_RegisterSetting");
	MarkNativeAsOptional("Kraken_GetSettingInt");
	MarkNativeAsOptional("Kraken_GetSettingFloat");
	MarkNativeAsOptional("Kraken_GetSettingString");
	MarkNativeAsOptional("Kraken_SetSettingInt");
	MarkNativeAsOptional("Kraken_SetSettingFloat");
	MarkNativeAsOptional("Kraken_SetSettingString");
	
	MarkNativeAsOptional("Kraken_RegisterServerSetting");
	MarkNativeAsOptional("Kraken_GetServerSettingInt");
	MarkNativeAsOptional("Kraken_GetServerSettingFloat");
	MarkNativeAsOptional("Kraken_GetServerSettingString");
	MarkNativeAsOptional("Kraken_SetServerSettingInt");
	MarkNativeAsOptional("Kraken_SetServerSettingFloat");
	MarkNativeAsOptional("Kraken_SetServerSettingString");
	
	MarkNativeAsOptional("Kraken_GetMapID");
	
	MarkNativeAsOptional("Kraken_GetUserID");
	MarkNativeAsOptional("Kraken_GetUserSessionID");
}

stock bool Kraken_IsValidClient(int iClient) 
{
	return (iClient >= 1 && iClient <= MaxClients && IsClientConnected(iClient) && IsClientInGame(iClient) && !IsFakeClient(iClient) && !IsClientSourceTV(iClient));
}

stock Kraken_GetClientOfUserID(int iUserID)
{
	for (int iClient = 1; iClient <= MaxClients; iClient++)
	{
		if(!Kraken_IsValidClient(iClient))
			continue;
		
		if(Kraken_GetUserID(iClient) == iUserID)
			return iClient;
	}
	
	return -1;
}