#define PLUGIN_VERSION "1.0"

#define DEBUG

public Plugin myinfo = {
	name = "Kraken - Player Stats",
	author = "Zipcore",
	description = "Simple event based player stats for Kraken",
	version = PLUGIN_VERSION,
	url = "www.zipcore.net",
};

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <kraken-core>
#include <kraken-playerstats>

Database g_dMain;

DBStatement g_hStatement = null;

char g_sServerName[64];

char g_sStats[][] = {
	"playtime",
	"spawns",
	"survived",
	"death",
	"suicide",
	"kill",
	"teamkill",
	"knifekill",
	"headshot",
	"wallbang",
	"assist",
	"shot",
	"dmg_in",
	"dmg_out",
	"hit",
	"hit_body",
	"hit_hs",
	"hit_upt",
	"hit_lot",
	"hit_larm",
	"hit_rarm",
	"hit_lleg",
	"hit_rleg",
	"mvp",
	"bomb_exp",
	"bomb_plt",
	"bomb_def",
	"bomb_drp",
	"flashed",
	"flash_team",
	"flash_enmy"
};

#define STATS_COUNT sizeof(g_sStats)
int g_iStats[MAXPLAYERS + 1][STATS_COUNT];

ArrayList g_aStats = null;

int g_iPlayTimeTemp[MAXPLAYERS + 1];

int g_iShotFiredTemp[MAXPLAYERS + 1];

int g_iDamageOutTemp[MAXPLAYERS + 1];
int g_iDamageInTemp[MAXPLAYERS + 1];

int g_iHitTemp[MAXPLAYERS + 1];
int g_iHitGroupTemp[MAXPLAYERS + 1][8];

int g_iFlashedTemp[MAXPLAYERS + 1];
int g_iFlashedTeamTemp[MAXPLAYERS + 1];
int g_iFlashedEnemyTemp[MAXPLAYERS + 1];

bool g_bClientLoaded[MAXPLAYERS + 1];

Handle g_fwdOnStatsLoaded;

bool g_bKrakenCore;
bool g_bLate;

#include "kraken-playerstats/stocks.sp"
#include "kraken-playerstats/temp.sp"
#include "kraken-playerstats/events.sp"
#include "kraken-playerstats/natives.sp"

public APLRes AskPluginLoad2(Handle hMyself, bool bLate, char[] sError, int iMaxError) 
{
	RegPluginLibrary("kraken-playerstats");
	
	g_aStats = new ArrayList(10);
	
	for (int i = 0; i < STATS_COUNT; i++)
		g_aStats.PushString(g_sStats[i]);
	
	g_fwdOnStatsLoaded = CreateGlobalForward("Kraken_PlayerStats_OnStatsLoaded", ET_Ignore, Param_Cell);
	
	CreateNative("Kraken_PlayerStats_GetStatsCount", Native_GetStatsCount);
	CreateNative("Kraken_PlayerStats_GetStatsID", Native_GetStatsID);
	CreateNative("Kraken_PlayerStats_GetStatsName", Native_GetStatsName);
	CreateNative("Kraken_PlayerStats_StatsLoaded", Native_StatsLoaded);
	CreateNative("Kraken_PlayerStats_GetStats", Native_GetStats);
	
	Kraken_MarkNativesAsOptional();
	Kraken_PlayerStats_MarkNativesAsOptional();
	
	return APLRes_Success;
}

public void OnPluginStart() 
{
	if(LibraryExists("kraken-core"))
		g_bKrakenCore = true;
	
	CreateTimer(1.0, Timer_UpdatePlaytime, _, TIMER_REPEAT);
	CreateTimer(10.0, Timer_UpdateTemp, _, TIMER_REPEAT);
	
	HookEvents();
	
	if(g_bLate)
	{
		g_bLate = false;
		CreateTable();
	}
}

public void OnLibraryAdded(const char[] name)
{
	if(StrEqual(name, "kraken-core"))
		g_bKrakenCore = true;
}

public void OnLibraryRemoved(const char[] name)
{
	if(StrEqual(name, "kraken-core"))
		g_bKrakenCore = false;
}

/* Database */

public void Kraken_OnDatabaseConnected(Database dDatabase, int iSID, int iGID, char[] sServerName)
{
	g_dMain = dDatabase;
	strcopy(g_sServerName, sizeof(g_sServerName), sServerName);
	
	CreateTable();
}

void CreateTable()
{
	if(!g_bKrakenCore || !Kraken_IsDatabaseLoaded())
		return;
	
	g_dMain = Kraken_GetDatabase();
	Kraken_PushQuery("CREATE TABLE IF NOT EXISTS `player_stats` (uid INT NOT NULL, sid INT NOT NULL, type varchar(10) NOT NULL, value INT NOT NULL, PRIMARY KEY (`uid`, `sid`, `type`), INDEX sid_index USING BTREE (sid), INDEX type_index USING BTREE (type), INDEX value_index USING BTREE (value));");
}

/* Player */

public void OnClientDisconnect(int iClient) 
{
	ResetPlayer(iClient);
}

public void Kraken_OnClientLoaded(int iClient, int iUserID, int iSessionID) 
{
	g_iPlayTimeTemp[iClient] = 0;
	
	char sBuffer[512];
	
	/* Get Stats */
	
	int iGroupID = Kraken_GetServerGroupID();
	int iServerID = Kraken_GetServerID();
	
  	#if defined DEBUG
		PrintToServer("Kraken_OnClientLoaded: %N (Server: %i)", iClient, iServerID);
	#endif	
	
	if(iGroupID > 0)
		Format(sBuffer, sizeof(sBuffer), "SELECT `type`,`value` FROM `player_stats` JOIN servers ON servers.sid = player_stats.sid WHERE uid = '%i' AND servers.gid = '%i' ORDER BY type ASC;", iUserID, iGroupID);
	else if(iServerID > 0) Format(sBuffer, sizeof(sBuffer), "SELECT `type`,`value` FROM `player_stats` WHERE uid = '%i' ORDER BY type ASC;", iUserID, iServerID);
	
	SQL_TQuery(g_dMain, Callback_LoadClientStats, sBuffer, GetClientUserId(iClient), DBPrio_High);
	
	/* Get ranks */
	
	for (int i = 0; i < STATS_COUNT; i++)
	{
		if(g_iStats[iClient][i] <= 0)
			continue;
		
		g_iStats[iClient][i] = 0;
	}
}

public void Callback_LoadClientStats(Handle hOwner, Handle hHndl, char[] sError, int iUserId) 
{
	if (hHndl == null) 
	{
		LogError("(LoadClientStats) - %s", sError);
		return;
	}	
	
	int iClient;

	iClient = GetClientOfUserId(iUserId);
	
  	#if defined DEBUG
		PrintToServer("Callback_LoadClientStats(%i)", iClient);
	#endif
	
	if (!Kraken_IsValidClient(iClient))
		return; // Player disconnected
	
	int iUID = Kraken_GetUserID(iClient);
	if(iUID <= 0) 
		return; // This should not happen
	
	ResetPlayer(iClient);
	
	while (SQL_FetchRow(hHndl)) 
	{
		char sType[10];
		SQL_FetchString(hHndl, 0, sType, sizeof(sType));
		
		int iStatsID = GetStatsID(sType);
		if(iStatsID == -1)
			continue;
		
		int iValue = SQL_FetchInt(hHndl, 1);
		
		g_iStats[iClient][iStatsID] += iValue;
	}
	
	g_bClientLoaded[iClient] = true;
	
	Call_StartForward(g_fwdOnStatsLoaded);
	Call_PushCell(iClient);
	Call_Finish();
}

/* Playtime */

public Action Timer_UpdatePlaytime(Handle hTimer) 
{
	for (int iClient = 1; iClient <= MaxClients; iClient++) 
	{
		if (!Kraken_IsValidClient(iClient)) 
			continue;
		
		g_iPlayTimeTemp[iClient]++;
		
		if(g_iPlayTimeTemp[iClient] < 60)
			continue;
		
		g_iPlayTimeTemp[iClient] -= 60;
	
	  	#if defined DEBUG
			PrintToServer("Timer_UpdatePlaytime(%N)", iClient);
		#endif
		
		UpdateStats(iClient, "playtime", 1);
	}
}

public Action Timer_UpdateTemp(Handle hTimer) 
{
	for (int iClient = 1; iClient <= MaxClients; iClient++) 
	{
		if (!Kraken_IsValidClient(iClient)) 
			continue;
		
		UpdateTemp(iClient);
	}
}

/* Stats */

void ResetPlayer(int iClient)
{
	g_iPlayTimeTemp[iClient] = 0;
	
	g_iShotFiredTemp[iClient] = 0;

	g_iDamageOutTemp[iClient] = 0;
	g_iDamageInTemp[iClient] = 0;

	g_iHitTemp[iClient] = 0;
	
	for (int i = 0; i < 8; i++)
		g_iHitGroupTemp[iClient][i] = 0;
	
	g_iFlashedTemp[iClient] = 0;
	g_iFlashedTeamTemp[iClient] = 0;
	g_iFlashedEnemyTemp[iClient] = 0;
	
	for (int i = 0; i < STATS_COUNT; i++)
		g_iStats[iClient][i] = 0;
	
	g_bClientLoaded[iClient] = false;
}

bool UpdateStats(int iClient, char[] sType, int iAmount)
{
  	#if defined DEBUG
		PrintToServer("UpdateStats(%N) Type: %s Value: %i", iClient, sType, iAmount);
	#endif
	
	if(!g_bKrakenCore || !Kraken_IsDatabaseLoaded())
		return;
	
	int iServerID = Kraken_GetServerID();
	
	if(iServerID <= 0)
		return;
	
	int iStatsID = GetStatsID(sType);
	if(iStatsID == -1)
	{
		LogError("Stats type not found \"%s\"", sType);
		return;
	}
	
	if(!Kraken_IsValidClient(iClient))
		return;
	
	int iUID = Kraken_GetUserID(iClient);
	if(iUID <= 0) 
		return;
	
	g_iStats[iClient][iStatsID] += iAmount;
	
	if(InitStatement())
	{
		SQL_BindParamInt(g_hStatement, 0, iUID, true);
		SQL_BindParamInt(g_hStatement, 1, iServerID, true);
		SQL_BindParamString(g_hStatement, 2, sType, false);
		SQL_BindParamInt(g_hStatement, 3, iAmount, true);
		SQL_BindParamInt(g_hStatement, 4, iAmount, true);
		if(!SQL_Execute(g_hStatement))
			LogError("Could not send binded params");
	}
	
	char sBuffer[256];
	Format(sBuffer, sizeof(sBuffer), "INSERT INTO `player_stats` VALUES (%i, %i, '%s', %i) ON DUPLICATE KEY UPDATE value = value + %i;", iUID, iServerID, sType, iAmount, iAmount);
	Kraken_PushQuery(sBuffer);
}

bool InitStatement()
{
	if(g_hStatement == null)
	{
		char sError[128];
		g_hStatement = SQL_PrepareQuery(g_dMain, "INSERT INTO `player_stats` VALUES (?, ?, '?', ?) ON DUPLICATE KEY UPDATE value = value + ?;", sError, sizeof(sError));
	
		if(g_hStatement == null)
		{
			LogError("Can't prepare query: %s", sError);
			return false;
		}
		
		return true;
	}
	
	return false;
}

public int GetStatsID(char[] name)
{
	return FindStringInArray(g_aStats, name);
}