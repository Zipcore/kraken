 #define PLUGIN_VERSION "1.0"

#define DEBUG

#include <sourcemod>
#include <sdkhooks>
#include <kraken-core>

bool g_bKrakenCore;
bool g_bLate;

public Plugin myinfo =
{
	name = "Kraken - Chat logger",
	author = "Zipcore",
	description = "Simple chat logger for Kraken",
	version = PLUGIN_VERSION,
	url = "www.zipcore.net",
};

public APLRes AskPluginLoad2(Handle hMyself, bool bLate, char[] sError, int iMaxError) 
{
	RegPluginLibrary("kraken-chat-logger");
	Kraken_MarkNativesAsOptional();
	
	g_bLate = bLate;

	return APLRes_Success;
}

public void OnPluginStart() 
{
	if(LibraryExists("kraken-core"))
		g_bKrakenCore = true;
	
	ChatHooks();
	
	if(g_bLate)
	{
		g_bLate = false;
		CreateTable();
	}
}

public void OnLibraryAdded(const char[] name)
{
	if(StrEqual(name, "kraken-core"))
		g_bKrakenCore = true;
}

public void OnLibraryRemoved(const char[] name)
{
	if(StrEqual(name, "kraken-core"))
		g_bKrakenCore = false;
}

public void Kraken_OnDatabaseConnected(Database dDatabase, int iSID, int iGID, char[] sServerName)
{
	CreateTable();
}

void CreateTable()
{
	if(!g_bKrakenCore || !Kraken_IsDatabaseLoaded())
		return;
	
	Kraken_PushQuery("CREATE TABLE IF NOT EXISTS `chat_log` (id INT NOT NULL AUTO_INCREMENT, sid INT NOT NULL, uid INT NOT NULL, usid INT NOT NULL, cmd VARCHAR(32) NOT NULL, msg VARCHAR(256) NOT NULL, time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`), INDEX sid_index USING BTREE (sid), INDEX uid_index USING BTREE (uid), INDEX usid_index USING BTREE (usid));");
}

void ChatHooks()
{
	AddCommandListener(SMSay, "sm_say");
	AddCommandListener(SMSay, "sm_chat");
	AddCommandListener(SMSay, "sm_csay");
	AddCommandListener(SMSay, "sm_tsay");
	AddCommandListener(SMSay, "sm_msay");
	AddCommandListener(SMSay, "sm_hsay");
	AddCommandListener(SMSay, "sm_psay");
}

public Action SMSay(int iClient, const char[] sCmd, int args)
{
	char sArgs[256];
	GetCmdArgString(sArgs, sizeof(sArgs));
	LogChatMsg(iClient, sCmd, sArgs);
}

public void OnClientSayCommand_Post(int iClient, const char[] sCmd, const char[] sArgs)
{
	if(!IsChatTrigger())
		LogChatMsg(iClient, sCmd, sArgs);
}

void LogChatMsg(int iClient, const char[] sCmd, const char[] sArgs)
{
	if (strlen(sArgs) == 0)
		return;
	
	if(!g_bKrakenCore || !Kraken_IsDatabaseLoaded())
		return;
		
	int iServerID = Kraken_GetServerID();
	if(iServerID <= 0)
		return;
	
	char sQuery[2048];
	if (Kraken_IsValidClient(iClient))
	{
		int iUserID = Kraken_GetUserID(iClient);
		int iUserSessionID = Kraken_GetUserSessionID(iClient);
		
		if(iUserID <= 0 || iUserSessionID <= 0)
			return;
		
		Format(sQuery, sizeof(sQuery), "INSERT INTO `chat_log` (sid,uid,usid,cmd,msg) VALUES (%i, %i, %i, '%s', '%s');", iServerID, iUserID, iUserSessionID, sCmd, sArgs);
		Kraken_PushQuery(sQuery);
		return;
	}
	
	Format(sQuery, sizeof(sQuery), "INSERT INTO `chat_log` (sid,uid,usid,cmd,msg) VALUES (%i, 0, 0, '%s', '%s');", iServerID, sCmd, sArgs);
	Kraken_PushQuery(sQuery);
}